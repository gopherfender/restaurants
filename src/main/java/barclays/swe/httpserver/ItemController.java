package barclays.swe.httpserver;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemController {
    private ItemRepository repository;
    private MenuRepository menuRepository;

    public ItemController(ItemRepository repository, MenuRepository menuRepository) {
        this.repository = repository;
        this.menuRepository = menuRepository;
    }

    @GetMapping("/restaurants/{restaurant_id}/menus/{menu_id}/items/{item_id}")
    public Item getItem(@PathVariable Integer item_id) {
        return repository.findById(item_id).get();
    }

    @PostMapping("/restaurants/{restaurant_id}/menus/{menu_id}/items")
    public Item addItem(@RequestBody Item itemDetails, @PathVariable Integer menu_id) {
        Menu menu = menuRepository.findById(menu_id).get();
        itemDetails.setMenu(menu);
        return repository.save(itemDetails);
    }

    @DeleteMapping("/restaurants/{restaurant_id}/menus/{menu_id}/items/{id}")
    public void deleteItem(@PathVariable Integer id) {
        repository.deleteById(id);
    }

    @PutMapping("/restaurants/{restaurant_id}/menus/{menu_id}/items/{id}")
    public Item updateItem(@PathVariable Integer id, @RequestBody Item itemUpdate) {
        return repository.findById(id).map(item -> {
            item.setName(itemUpdate.getName());
            item.setPrice(itemUpdate.getPrice());
            return repository.save(item);
        }).orElseThrow();
    }

}
