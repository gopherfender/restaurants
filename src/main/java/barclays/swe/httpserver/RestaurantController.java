package barclays.swe.httpserver;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestaurantController {
    private RestaurantRepository repository;

    public RestaurantController(RestaurantRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/restaurants")
    public List<Restaurant> getRestaurants() {
        return this.repository.findAll();
    }

    @GetMapping("/restaurants/{id}")
    public Restaurant getOne(@PathVariable Integer id) {
        return repository.findById(id).get();
    }

    @PostMapping("/restaurants")
    public Restaurant createRestaurant(@RequestBody Restaurant newRestaurant) {
        return repository.save(newRestaurant);
    }

    @DeleteMapping("/restaurants/{id}")
    public Restaurant deleteRestaurant(@PathVariable int id) {
        Restaurant restaurant = repository.findById(id).get();
        repository.deleteById(id);
        return restaurant;
    }

    @PutMapping("restaurants/{id}")
    public Restaurant updateOne(@PathVariable Integer id, @RequestBody Restaurant restaurantUpdate) {
        return repository.findById(id).map(restaurant -> {
            restaurant.setName(restaurantUpdate.getName());
            restaurant.setImageURL(restaurantUpdate.getImageURL());
            return repository.save(restaurant);
        }).orElseThrow();
    }

    @PatchMapping("/restaurants/name")
    public ResponseEntity<Restaurant> updateName(@RequestParam int id, @RequestParam String name) {
        try {
            Restaurant restaurant = repository.findById(id).get();
            restaurant.setName(name);
            return new ResponseEntity<Restaurant>(repository.save(restaurant), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/restaurants/imageURL")
    public ResponseEntity<Restaurant> updateImageUrl(@RequestParam int id, @RequestParam String imageURL) {
        try {
            Restaurant restaurant = repository.findById(id).get();
            restaurant.setImageURL(imageURL);
            return new ResponseEntity<Restaurant>(repository.save(restaurant), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
