package barclays.swe.httpserver;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MenuController {
    private MenuRepository repository;
    private RestaurantRepository restaurantRepository;

    public MenuController(MenuRepository repository, RestaurantRepository restaurantRepository) {
        this.repository = repository;
        this.restaurantRepository = restaurantRepository;
    }

    @GetMapping("/restaurants/{restaurant_id}/menus/{menu_id}")
    public Menu getMenu(@PathVariable Integer menu_id) {
        return repository.findById(menu_id).get();
    }

    @PostMapping("/restaurants/{restaurant_id}/menus")
    public Menu addMenu(@RequestBody Menu menuDetails, @PathVariable Integer restaurant_id) {
        Restaurant restaurant = restaurantRepository.findById(restaurant_id).get();
        menuDetails.setRestaurant(restaurant);
        return repository.save(menuDetails);
    }

    @DeleteMapping("/restaurants/{restaurant_id}/menus/{id}")
    public void deleteMenu(@PathVariable Integer id) {
        repository.deleteById(id);
    }

    @PutMapping("/restaurants/{restaurant_id}/menus/{id}")
    public Menu updateOne(@PathVariable Integer id, @RequestBody Menu menuUpdate) {
        return repository.findById(id).map(menu -> {
            menu.setTitle(menuUpdate.getTitle());
            return repository.save(menu);
        }).orElseThrow();
    }
}
