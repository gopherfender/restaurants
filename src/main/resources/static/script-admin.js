let data = [];
const state = {
  restaurantOpen: null,
  menuOpen: null,
  newRestaurantOpen: null,
  newMenuOpen: null,
  newMenu: null,
};

fetchAll();

//Get fresh DB data and rebuild page
function fetchAll() {
  fetch("/restaurants")
    .then((response) => response.json())
    .then((_data) => {
      data = _data;
      console.log(data);
      renderRestaurants();
    })
    .catch((err) => console.log(err));
}

//Generate Restaurant Grid HTML
function renderRestaurants() {
  const resultDiv = document.getElementById("result-div");
  resultDiv.innerHTML = "";
  const content = data
    .map((entry, i) => {
      return /*html*/ `<div class="restaurant-card">
            <div class="image-div">
              <img class="restaurant-image" src="${entry.imageURL}">
            </div>
            <div class="text-div">
              <p>${entry.name}</p>
              <!-- change this to editRestaurant -->
              <button onclick="displayRestaurant(${i})" class="restaurant-btn" role="button">Edit</button>
            </div>
          </div>`;
    })
    .join("");
  resultDiv.innerHTML = content;

  if (state.restaurantOpen) {
    renderMenus();
  } else if (state.newRestaurantOpen) {
    renderNewRestaurant();
  } else {
    const modalEl = document.getElementById("modal");
    modalEl.innerHTML = "";
  }
}

//Generate modal for selected Restaurant with grid of Menus
function renderMenus() {
  const modalContent = /*html*/ `
    <section class="rest-modal-bg">
        <article>
        <button class="back-btn" onclick="closeRestaurant()">👈</button>
          <div class="modal-card-header">
            <div class="modal-card-left">
            <form class="update-rest-form" onsubmit="event.preventDefault();updateRestaurant(${
              state.restaurantOpen.id
            }, this)">
                <label>Name</label>
                <input name="name" id="restaurant-name-input" value="${
                  state.restaurantOpen.name
                }" required><br>
                <label>Image URL</label>
                <input name="image" id="restaurant-imageURL-input" value="${
                  state.restaurantOpen.imageURL
                }" required><br>
                <button class="button" id="change-name-btn" type="submit">Update</button>
              </form>
            </div>
            <div class="modal-card-centre">
            <button class="button" type="button" onclick="addMenu(${
              state.restaurantOpen.id
            })">Add Menu</button>
                <button class="button" type="button" onclick="deleteRestaurant(${
                  state.restaurantOpen.id
                })">DELETE</button>
            </div>
            <div class="modal-card-right">
              <img class="rest-focus-img" src="${
                state.restaurantOpen.imageURL
              }">
            </div>
          </div>
          <div class="modal-menu-grid">
            ${state.restaurantOpen.menus
              .map((menu, i) => {
                return `
                <div class="modal-menu-card">
                  <h2 class="menu-card-title">${menu.title}</h2>
                  <button class="menu-btn" role="button" onclick="displayMenu(${i})">Edit</button>
                </div>
              `;
              })
              .join("")}
          </div>
          

          
        </article>
    </section>
        `;

  const modalEl = document.getElementById("modal");
  modalEl.innerHTML = modalContent;

  if (state.menuOpen) {
    renderMenu();
  } else if (state.newMenuOpen) {
    renderNewMenu();
  } else {
    const innerModalEl = document.getElementById("inner-modal");
    innerModalEl.innerHTML = "";
  }
}

//Generate inner modal for selected menu over selected Restaurant
function renderMenu() {
  const innerModalContent = /*html*/ `
    <section class="menu-modal-bg">
      <article>
        <button class="back-btn" onclick="closeMenu()">👈</button>
        <h3 class="menu-modal-title">${state.menuOpen.title}</h3>
        <div class="item-list">
          ${state.menuOpen.items
            .map((item) => {
              return `
            <p>${item.name}</p>
            <p>${item.price}</p>
            `;
            })
            .join("")}
        </div>
      </article>
    </section>
    `;
  const innerModalEl = document.getElementById("inner-modal");
  innerModalEl.innerHTML = innerModalContent;
}

//Button method to set current restaurant and rebuild page
function displayRestaurant(index) {
  console.log(data[index]);
  state.restaurantOpen = data[index];
  renderRestaurants();
}

//Button method to set current menu and rebuild page
function displayMenu(index) {
  console.log(state.restaurantOpen.menus[index]);
  state.menuOpen = state.restaurantOpen.menus[index];
  renderRestaurants();
}

//deselect restaurant and rebuild
function closeRestaurant() {
  state.restaurantOpen = null;
  renderRestaurants();
}

//deselect menu and rebuild
function closeMenu() {
  state.menuOpen = null;
  renderRestaurants();
}

//Cancel new Restaurant form and rebuild
function cancelNewRestaurant() {
  state.newRestaurantOpen = false;
  renderRestaurants();
}

//Cancel new menu form and rebuild
function cancelNewMenu() {
  state.newMenuOpen = false;
  state.newMenu = null;
  renderRestaurants();
}

//Button method to set new Restaurant flag and rebuild page
function addRestaurant() {
  state.newRestaurantOpen = true;
  renderNewRestaurant();
}

//Button method to set new menu flag and rebuild page
function addMenu(restaurant_id) {
  state.newMenuOpen = true;
  renderNewMenu(restaurant_id);
}

//Generate inner modal for new Menu form
function renderNewMenu(restaurant_id) {
  const modalContent = /*html*/ `
  <section class="rest-modal-bg">
      <article>
        <div class="new-menu-form">
          <form name="form" onsubmit="event.preventDefault();">
          <main>
            <h3>Enter New Menu</h3>
            <input name="title" onchange="updateNewMenuTitle(this)" placeholder="Menu Title">
            <div id="new-items">
              <input name="itemName" placeholder="Item Name">
              <input name="price" placeholder="Item Price">
              <button id="add-item-btn" onclick="addItem(itemName.value, price.value)"> + </button>
            </div>
          </main>
          <footer>
            <button id="add-menu-btn" onclick="postMenu()">Save</button>
            <button id="cancel-btn" onclick="cancelNewMenu()">Cancel</button>
          </footer>
          </form>
        </div>     
      </article>
  </section>
  `;

  const modalEl = document.getElementById("inner-modal");
  modalEl.innerHTML = modalContent;
}

//Initiate NewMenu if required and update title
function updateNewMenuTitle(title) {
  if (!state.newMenu) {
    state.newMenu = {
      title: "",
      newItems: [],
    };
  }
  state.newMenu.title = title.value;
}

//Add New item line to new menu modal and rebuild
function addItem(name, price) {
  if (!state.newMenu) {
    state.newMenu = {
      title: "",
      newItems: [],
    };
  }

  state.newMenu.newItems.push({ name, price });

  const itemsDiv = document.getElementById("new-items");

  const listHtml = /*html*/ `
    ${state.newMenu.newItems
      .map((item, index) => {
        return `<div class="item-list">
                  <p>${item.name} £${item.price}</p> 
                  <button onclick="removeItemFromState(${index})">delete</button>
                </div>       
        `;
      })
      .join("")}
      <div class="new-item-line">
        <input name="itemName" placeholder="Item Name">
        <input name="price" placeholder="Item Price">
        <button id="add-item-btn" onclick="addItem(itemName.value, price.value)"> + </button>
      </div>
  `;

  itemsDiv.innerHTML = listHtml;
}

//Remove item from new Menu list and rebuild
function removeItemFromState(index) {
  state.newMenu.newItems.splice(index, 1);

  const itemsDiv = document.getElementById("new-items");

  const listHtml = /*html*/ `
    ${state.newMenu.newItems
      .map((item, index) => {
        return `<div class="item-list">
                  <p>${item.name} £${item.price}</p> 
                  <button onclick="removeItemFromState(${index})">delete</button>
                </div>       
        `;
      })
      .join("")}
      <div class="new-item-line">
        <input name="itemName" placeholder="Item Name">
        <input name="price" placeholder="Item Price">
        <button id="add-item-btn" onclick="addItem(itemName.value, price.value)"> + </button>
      </div>
  `;

  itemsDiv.innerHTML = listHtml;
}

//generate modal for new restaurant form and rebuild
function renderNewRestaurant() {
  const modalContent = /*html*/ `
    <section class="rest-modal-bg">
      <article class="new-restaurant">
        <h4>Add Restaurant</h4>
        <div class="new-rest-div">
          <form id="new-rest-form" onsubmit="event.preventDefault();postRestaurant(name.value, image.value);">
            <label>Name</label>
            <input name="name" required</input>
            <label>Image URL</label>
            <input name="image" required></input>
          <button class="button" role="button">
            Save
          </button>
          <button
            class="button" role="button" onclick="cancelNewRestaurant()">Cancel
            </button>
          </form>        
        </div>
      </article>
    </section>
    `;
  const modalEl = document.getElementById("modal");
  modalEl.innerHTML = modalContent;
}

//HTTP POST request for new restaurant entry
function postRestaurant(name, imageURL) {
  fetch("/restaurants", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ name, imageURL }),
  })
    .then((response) => response.json())
    .then((restaurant) => {
      state.newRestaurantOpen = false;
      fetchAll();
    })
    .catch(console.error);
}

//HTTP DELETE request for restaurant
function deleteRestaurant(id) {
  if (confirm("Are you sure you want to delete " + state.restaurantOpen.name)) {
    fetch("/restaurants/" + id, {
      method: "DELETE",
    })
      .then((next) => {
        state.restaurantOpen = null;
        fetchAll();
      })
      .catch(console.error);
  }
}

//HTTP PUT request to update restaurant
function updateRestaurant(id, form) {
  data = new FormData(form);
  name = data.get("name");
  imageURL = data.get("image");
  console.log(id, data.get("name"));
  fetch("/restaurants/" + id, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ name, imageURL }),
  })
    .then((response) => response.json())
    .then((restaurant) => {
      state.restaurantOpen = null;
      fetchAll();
    })
    .catch(console.error);
}

//HTTP POST request to add new menu
function postMenu() {
  const title = state.newMenu.title;

  fetch("/restaurants/" + state.restaurantOpen.id + "/menus", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ title }),
  })
    .then((response) => response.json())
    .then((response) => {
      console.log(response);
      postItems(response.id);
    })
    .catch(console.error);
}

function postItems(menuId) {
  console.log(menuId);
  for (item in state.newMenu.newItems) {
    setTimeout(() => {
      console.log(item);
      const name = item.name;
      const price = item.price;
      fetch(`/restaurants/${state.restaurantOpen.id}/menus/${menuId}/items/`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ name, price }),
      })
        .then((response) => response.json())
        .then((response) => {
          console.log(response);
        })
        .catch(console.error);
    }, 500);
  }
}
