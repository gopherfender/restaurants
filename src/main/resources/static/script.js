let data = [];
const state = {
  restaurantOpen: null,
  menuOpen: null,
};

fetch("/restaurants")
  .then((response) => response.json())
  .then((_data) => {
    data = _data;
    console.log(data);
    renderRestaurants();
  })
  .catch((err) => console.log(err));

function renderRestaurants() {
  const resultDiv = document.getElementById("result-div");
  resultDiv.innerHTML = "";
  const content = data
    .map((entry, i) => {
      return `<div class="restaurant-card">
            <div class="image-div">
              <img class="restaurant-image" src="${entry.imageURL}">
            </div>
            <div class="text-div">
              <p>${entry.name}</p>
              <button onclick="displayRestaurant(${i})" class="restaurant-btn" role="button">More</button>
            </div>
          </div>`;
    })
    .join("");
  resultDiv.innerHTML = content;

  if (state.restaurantOpen) {
    renderMenus();
  } else {
    const modalEl = document.getElementById("modal");
    modalEl.innerHTML = "";
  }
}

function displayRestaurant(index) {
  console.log(data[index]);
  state.restaurantOpen = data[index];
  renderRestaurants();
}

function displayMenu(index) {
  console.log(state.restaurantOpen.menus[index]);
  state.menuOpen = state.restaurantOpen.menus[index];
  renderRestaurants();
}

function closeRestaurant() {
  state.restaurantOpen = null;
  renderRestaurants();
}

function closeMenu() {
  state.menuOpen = null;
  renderRestaurants();
}

function renderMenus() {
  const modalContent = `
    <section class="rest-modal-bg">
        <article>
        <button class="back-btn" onclick="closeRestaurant()">👈</button>
          <div class="modal-card-header">
            <h3 class="rest-card-title">${state.restaurantOpen.name}</h3>
            <img class="rest-focus-img" src="${state.restaurantOpen.imageURL}">
          </div>
          <div class="modal-menu-grid">
            ${state.restaurantOpen.menus
              .map((menu, i) => {
                return `
                <div class="modal-menu-card">
                  <h2 class="menu-card-title">${menu.title}</h2>
                  <button class="menu-btn" role="button" onclick="displayMenu(${i})">View Menu</button>
                </div>
              `;
              })
              .join("")}
          </div>

          
        </article>
    </section>
        `;

  const modalEl = document.getElementById("modal");
  modalEl.innerHTML = modalContent;

  if (state.menuOpen) {
    renderMenu();
  } else {
    const innerModalEl = document.getElementById("inner-modal");
    innerModalEl.innerHTML = "";
  }
}

function renderMenu() {
  const innerModalContent = `
    <section class="menu-modal-bg">
      <article>
        <button class="back-btn" onclick="closeMenu()">👈</button>
        <h3 class="menu-modal-title">${state.menuOpen.title}</h3>
        <div class="item-list">
          ${state.menuOpen.items
            .map((item) => {
              return `
            <p>${item.name}</p>
            <p>${item.price}</p>
            `;
            })
            .join("")}
        </div>
      </article>
    </section>
    `;
  const innerModalEl = document.getElementById("inner-modal");
  innerModalEl.innerHTML = innerModalContent;
}
